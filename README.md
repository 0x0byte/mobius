# Mobius Web

Typescript port of the Mobius Text Adventure Framework originally written in Haxe.

## Build & Run

Currently the build artifacts are not commited to git. You must have Node.js installed in order to build the project.
After building you can open `dist/index.html` in a browser to run the game.

Build has been tested with Node.js LTS Erbium v12.18.3.

## Notes

In an attempt to make this framework more data driven and re-usable by anyone to create their own text adventure game all game data is stored under `src/game`.

The framework library will only contain the abstract objects to construct the game. All rooms, items, events, scripts, etc. will be stored in the game module.

## Todo

- [ ] Unit Testing
  - [x] Setup Mocha & Webpack for basic library testing
  - [ ] Configure unit tests for frontend - not just core library.
    - [ ] Production web player
    - [ ] Debug inspector
