const path = require("path");
// const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

module.exports = {
  mode: "development",
  entry: "./src/all.spec.ts",
  devtool: "inline-source-map",

  // devServer: {
  //   contentBase: "./dist",
  //   hot: true,
  // },

  // watchOptions: {
  //   ignored: ["**/node_modules", "dist"],
  // },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.s[ac]ss$/i,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      // {
      //   test: /\.spec\.ts/,
      //   use: ["mocha-loader", "ts-loader"],
      //   exclude: /node_modules/,
      // },
    ],
  },

  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    fallback: {
      assert: require.resolve("assert/"),
    },
    modules: [path.join(__dirname, "./src"), "node_modules"],
  },

  output: {
    filename: "tests.js",
    path: path.resolve(__dirname, "dist"),
  },

  plugins: [
    // new HtmlWebpackPlugin({
    //   title: "Mobius Web",
    // }),

    // not sure what needed this but I've left it in. Need to check git logs to be sure.
    new webpack.DefinePlugin({
      "process.env.NODE_DEBUG": JSON.stringify(process.env.NODE_DEBUG),
    }),
  ],

  experiments: {
    topLevelAwait: true,
  },
};
