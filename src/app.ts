import "@fortawesome/fontawesome-free/js/all.js";
import "scss/mobius.scss";

import game from "game/game";

// we have to do a relative import because there is a node_module with the same name
import Inspector from "./inspector";

const IS_DEV = true;

if (IS_DEV) {
  const inspector = new Inspector(game);
}

game.run();
