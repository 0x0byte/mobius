// import game from "./game/game";
// import io from "./io";
// import Mobius, { MobiusObject } from "./mobius";
// import { ActionContext, ActionResult, Inspectable, Usable } from "./actions";
// import { assert } from "./utils";

// export type DoorConnection = Set<Room>;

// export interface IDoor extends MobiusObject, Inspectable, Usable {
//   cross(): boolean;
//   lock(): boolean;
//   unlock(): boolean;
// }

// export interface Key extends MobiusObject {}

// // TODO: Doors should act as one way portals to another room. They are specialized game objects which when interacted with change the game state current room.
// export class Door extends MobiusObject implements IDoor {
//   #dest: Room;
//   #isLocked: boolean = false;
//   #key?: Key;

//   constructor(dest: Room, key?: Key) {
//     super();
//     this.#dest = dest;
//     this.#key = key;
//   }

//   // IDoor

//   /**
//    * Attempts to exit the current room and enter the next.
//    * @returns  boolean success if the player crossed rooms.
//    */
//   cross() {
//     // Unable to cross door without it being unlocked
//     if (this.#isLocked) {
//       io.show("The door is locked.");
//       return false;
//     } else {
//       const prevRoom = game.state.room;
//       const nextRoom = this.#dest;
//       assert(prevRoom !== undefined);

//       // TODO: dispatch before and after room enter exit events
//       prevRoom.exit(nextRoom);
//       nextRoom.enter(prevRoom);
//       game.state.room = nextRoom;

//       // save users the trouble of inspecting a room after entering it
//       io.show(nextRoom.inspect());

//       return true;
//     }
//   }

//   /**
//    * @returns true if the door state is locked
//    */
//   lock(): boolean {
//     if (this.#isLocked) {
//       io.show("The door is already locked.");
//     } else if (this.#key && game.state.player.has(this.#key)) {
//       this.#isLocked = true;
//       io.show("You lock the door.");
//     }

//     return this.#isLocked;
//   }

//   /**
//    * @returns true if the door state is unlocked
//    */
//   unlock(): boolean {
//     if (!this.#isLocked) {
//       io.show("The door is already unlocked.");
//     } else if (this.#key && game.state.player.has(this.#key)) {
//       this.#isLocked = false;
//       io.show("You unlock the door.");
//     }

//     return this.#isLocked;
//   }

//   // Inspectable

//   inspect() {
//     return "Looks like a door.";
//   }

//   // Usable

//   use() {
//     this.cross();
//     return "";
//   }
// }

// export default abstract class Room extends MobiusObject implements Inspectable {
//   // NOTE: all room objects are contained in the context
//   #context = new ActionContext();

//   get context(): Readonly<ActionContext> {
//     return this.#context;
//   }

//   abstract inspect(): ActionResult;

//   /**
//    * Wrapper around ActionContext.find
//    * @param alias Object alias string
//    */
//   find(alias: string) {
//     return this.#context.find(alias);
//   }

//   // NOTE: we could do the room load / unloading from the enter / exit methods
//   enter(_prev: Room) {
//     if (!this.isInit) this.init();
//   }

//   exit(_next: Room) {}
// }
