// NOTE: using mocha loader prevented these from running
import assert from "assert";

import Mobius from "./mobius";
console.log(Mobius);

describe("Array", function () {
  describe("#indexOf()", function () {
    it("should return -1 when the value is not present", function () {
      assert.strictEqual([1, 2, 3].indexOf(4), -1);
    });
  });
});
