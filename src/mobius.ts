import io from "io";
// import Room from "./rooms";
import { assert, grey, red } from "utils";
import actions, {
  ActionContext,
  ActionResult,
  Inspectable,
  Usable,
} from "actions";

// FIXME: game should not be imported in mobius core, its defined by users creating
// their own games reference this core library.
// import game from "game/game";

/**
 * Abstract base class for common functions of Rooms, Items, Mobs, etc.
 */
export abstract class MobiusObject {
  // NOTE: not sure if storing all game objects used by the inspector as a static property of MobiusObject is the best way to handle this.
  // objects are referenced in the Inspector to display their properties and relations.
  static objects: MobiusObject[] = [];

  // NOTE: we could enforce naming of derived MobiusObjects by making it a constructor parameter.
  name = "[MobuisObject]";
  #parent: MobiusObject | null = null;
  #children = new Set<MobiusObject>();

  #isInit = false;

  constructor() {
    // TODO: register object with the system so it can be viewed in debug inspector.
    MobiusObject.objects.push(this);
  }

  get isInit(): boolean {
    return this.#isInit;
  }

  get parent(): MobiusObject | null {
    return this.#parent;
  }

  set parent(val: MobiusObject | null) {
    // FIXME: remove gets called multiple times, once here and again from inside the add method.
    this.#parent?.remove(this);
    val?.add(this);
  }

  get children(): Set<MobiusObject> {
    return this.#children;
  }

  get game(): Mobius {
    return Mobius.instance;
  }

  /**
   * Deferred initialization to prevent circular dependencies.
   */
  init(): void {
    this.#isInit = true;
  }

  /**
   * Checks if this has obj as a child.
   * @param obj
   */
  has(obj: MobiusObject): boolean {
    return this.children.has(obj);
  }

  /**
   * Reparents MobiusObject removing it from its old parent and adding it to this one.
   * @param obj
   */
  private add(obj: MobiusObject) {
    obj.#parent?.remove(obj);
    obj.#parent = this;
    this.children.add(obj);
  }

  /**
   * Orphans a MobiusObject if it is a child of this object.
   * Does nothing if obj is not a child of this.
   * @param obj The object to remove from this objects children.
   */
  private remove(obj: MobiusObject) {
    if (this.#children.delete(obj)) obj.#parent = null;
  }
}

/**
 * The serializable part of the game.
 * This can be used to create snapshots, saves, and undo for testing.
 */
export class MobiusState {
  player = new Player();
  room: Room = {} as Room;
}

/**
 * Dummy MobiusObject to store player related data.
 */
export class Player extends MobiusObject {
  name = "Player";
}

/**
 * Basic state machine with a init and update loop.
 */
export default abstract class Mobius {
  private static _instance: Mobius | null;
  state = new MobiusState();

  constructor() {
    if (Mobius._instance) {
      throw new Error("Attempted to initialize multiple Mobius instances.");
    }

    Mobius._instance = this;
  }

  /**
   * Get the ActionContext object of the current room.
   * @returns ActionContext of the current room.
   */
  get context(): Readonly<ActionContext> {
    return this.state.room?.context;
  }

  static get instance(): Mobius {
    console.log(Mobius._instance);
    if (!Mobius._instance)
      throw TypeError(
        "Mobius instance is null. It has been referenced before the game was initialized."
      );

    return Mobius._instance;
  }

  async run(): Promise<void> {
    try {
      this.start();
      // eslint-disable-next-line no-constant-condition
      while (true) {
        await this.update();
      }
    } catch (err) {
      console.error(err);
      io.show(red(err));
      io.show(red("EXITING GAME"));
    } finally {
      this.close();
    }
  }

  private start() {
    io.show(grey("Loading..."));
    const loadInterval = setInterval(() => io.show(grey("Loading...")), 5000);

    // adds initial room to the game state
    this.init();

    clearInterval(loadInterval);
    io.show(grey("Done Loading"));

    assert(this.state.room);
    this.state.room.enter({} as Room);

    // inspect our starter room since we never cross into it where this method is normally called
    io.show(this.state.room.inspect());
  }

  private async update() {
    // NOTE: should the result of an action be a change in game state?
    const command = await io.prompt("");
    const result = actions.perform(command);
    io.show(result);
  }

  abstract init(): void;
  abstract close(): void;
}

////////////////////////////////////////////////////////////////////////////////
// Moved from 'rooms.ts' to prevent circular dependencies.
//
// It would be nice to abstract out Rooms, Player, and Doors
// fro mthe core library.
////////////////////////////////////////////////////////////////////////////////

export type DoorConnection = Set<Room>;

export interface IDoor extends MobiusObject, Inspectable, Usable {
  cross(): boolean;
  lock(): boolean;
  unlock(): boolean;
}

type Key = MobiusObject;
// export interface Key extends MobiusObject {
// }

// TODO: Doors should act as one way portals to another room. They are specialized game objects which when interacted with change the game state current room.
export class Door extends MobiusObject implements IDoor {
  #dest: Room;
  #isLocked = false;
  #key?: Key;

  constructor(dest: Room, key?: Key) {
    super();
    this.#dest = dest;
    this.#key = key;
  }

  // IDoor

  /**
   * Attempts to exit the current room and enter the next.
   * @returns  boolean success if the player crossed rooms.
   */
  cross(): boolean {
    // Unable to cross door without it being unlocked
    if (this.#isLocked) {
      io.show("The door is locked.");
      return false;
    } else {
      const prevRoom = this.game.state.room;
      const nextRoom = this.#dest;
      assert(prevRoom !== undefined);

      // TODO: dispatch before and after room enter exit events
      prevRoom.exit(nextRoom);
      nextRoom.enter(prevRoom);
      this.game.state.room = nextRoom;

      // save users the trouble of inspecting a room after entering it
      io.show(nextRoom.inspect());

      return true;
    }
  }

  /**
   * @returns true if the door state is locked
   */
  lock(): boolean {
    if (this.#isLocked) {
      io.show("The door is already locked.");
    } else if (this.#key && this.game.state.player.has(this.#key)) {
      this.#isLocked = true;
      io.show("You lock the door.");
    }

    return this.#isLocked;
  }

  /**
   * @returns true if the door state is unlocked
   */
  unlock(): boolean {
    if (!this.#isLocked) {
      io.show("The door is already unlocked.");
    } else if (this.#key && this.game.state.player.has(this.#key)) {
      this.#isLocked = false;
      io.show("You unlock the door.");
    }

    return this.#isLocked;
  }

  // Inspectable

  inspect(): ActionResult {
    return "Looks like a door.";
  }

  // Usable

  use(): ActionResult {
    this.cross();
    return "";
  }
}

export abstract class Room extends MobiusObject implements Inspectable {
  // NOTE: all room objects are contained in the context
  #context = new ActionContext();

  get context(): Readonly<ActionContext> {
    return this.#context;
  }

  abstract inspect(): ActionResult;

  /**
   * Wrapper around ActionContext.find
   * @param alias Object alias string
   */
  find(alias: string): MobiusObject | undefined {
    return this.#context.find(alias);
  }

  // NOTE: we could do the room load / unloading from the enter / exit methods
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  enter(_prev: Room): void {
    if (!this.isInit) this.init();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  exit(_next: Room): void {
    // 
  }
}
