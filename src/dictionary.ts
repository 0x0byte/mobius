// A list of common words and synonyms for actions

type DictionaryItems = { [_: string]: Set<string> };

class Dictionary {
  constructor(words: DictionaryItems) {}
}

// No two words can exist in a dictionary
export default new Dictionary({
  examine: new Set(["examine", "e", "inspect", "see", "look", "l"]),
  inventory: new Set(["inventory", "i", "items"]),
  interact: new Set(["interact", "use", "f"]),
});
