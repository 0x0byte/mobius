// https://stackoverflow.com/questions/43084557/using-promises-to-await-triggered-events

// system like blocking io for web using async await
// TODO: wrap this up into a terminal emulator component that renders its own DOM

import EventEmitter from "events";
import { blue } from "./utils";

class IOEmitter extends EventEmitter {}

class Stdin {
  #ioEmitter = new IOEmitter();
  #element = document.getElementById("mobius-stdin");

  constructor() {
    // adapt the javascript key event into our simplified stdin event
    this.#element?.addEventListener("keydown", (event) => {
      if (event.key === "Enter" && event.target instanceof HTMLInputElement) {
        this.#ioEmitter.emit("mobius-stdin", event.target.value);
        event.target.value = "";
      }
    });
  }

  async readline(): Promise<string> {
    const result = await EventEmitter.once(this.#ioEmitter, "mobius-stdin");
    if (result.length > 0 && typeof result[0] === "string") {
      return result[0];
    }
    throw new Error("Unexpected result from Stdin.ioEmitter");
  }
}

class Stdout {
  #element = document.getElementById("mobius-stdout");

  writeline(msg: string) {
    const pEl = document.createElement("p");
    pEl.innerHTML = msg;
    this.#element?.appendChild(pEl);
    this.#element?.scrollTo(0, this.#element.scrollHeight);
  }
}

export default {
  stdin: new Stdin(),
  stdout: new Stdout(),

  show(msg: string) {
    this.stdout.writeline(msg);
  },

  async prompt(msg: string): Promise<string> {
    this.show(blue(msg));
    return await this.stdin.readline();
  },
};
