const ctx = require.context(".", true, /.+\.spec.ts$/);
ctx.keys().forEach(ctx);
export default ctx;
