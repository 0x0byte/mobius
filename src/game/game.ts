import Mobius from "../mobius";
import actions, {
  // helpAction,
  // inventoryAction,
  lookAction,
  takeAction,
  useAction,
} from "../actions";

import spaceShuttle from "./rooms/SpaceShuttle";
// import Room from "../rooms";

export class Game extends Mobius {
  init(): void {
    // TODO: action functions shouldn't be hard coded here since they are part of the game data.
    // actions.add(helpAction, new Set(["help", "h"]));
    actions.add(lookAction, ["look", "l"]);

    // TODO: define aliases for specific object types - rooms can have 'enter' as an alias for 'use'
    actions.add(useAction, ["use", "u"]);
    // actions.add(inventoryAction, new Set(["inventory", "i"]));
    actions.add(takeAction, ["take", "t"]);

    // starting room
    this.state.room = spaceShuttle;
  }

  // cleanup
  close(): void {
    //
  }
}

export default new Game();
