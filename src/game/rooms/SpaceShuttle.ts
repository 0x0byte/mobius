import { Door, Room } from "../../mobius";
import { ctx } from "../../actions";
// import Room, { Door } from "../../rooms";
import airlock from "./Airlock";
import excursionShip from "./ExcursionShip";

class SpaceShuttle extends Room {
  name = "Space Shuttle";

  airlockDoor = {} as Door;

  init() {
    this.airlockDoor = new Door(airlock);
    super.init();
  }

  inspect() {
    return `This small cramped ${ctx(
      "shuttle",
      this
    )} ferried you from the main ${ctx(
      "excursion ship",
      excursionShip
    )} to the Vault. Looking around you see a small fow of seats. There are also two doors, one labeled 'cockpit' and another labeled '${ctx(
      "airlock",
      this.airlockDoor
    )}'.`;
  }
}

export default new SpaceShuttle();
