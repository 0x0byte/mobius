import { ActionResult, ctx } from "actions";
import { MobiusObject, Room } from "mobius";
// import Room from "../../rooms";

/**
 * Testing the rewind mechanic. Player needs to be able to pick up several objects in the room, then talk to a person / interact with an object to initiate the rewind.
 */
class Reception extends Room {
  name = "Reception";

  inspect() {
    return `There are a few objects in the room. ${ctx(
      "Object A",
      {} as MobiusObject
    )}, ${ctx("Object B", {} as MobiusObject)}, ${ctx(
      "Object C",
      {} as MobiusObject
    )}. There is a ${ctx("person", {} as MobiusObject)} standing there.`;
  }
}

export default new Reception();
