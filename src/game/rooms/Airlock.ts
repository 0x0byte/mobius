import io from "../../io";
import { ctx, Inspectable } from "../../actions";
// import Room, { Door } from "../../rooms";
import reception from "./Reception";
import spaceShuttle from "./SpaceShuttle";
import { Door, Room } from "../../mobius";

class Airlock extends Room {
  name = "Airlock";

  // FIXME: testing if the way we default initialize our airlockdoor here affects what properties are on it
  shuttleDoor = {} as Door;
  stationDoor = {} as Door;

  init() {
    this.shuttleDoor = new Door(spaceShuttle);
    this.stationDoor = new Door(reception);

    super.init();
  }

  inspect() {
    return `Looking around the ${ctx(
      "airlock",
      this
    )} you see a rack with space suits. This area acts as the access to the outside of the station when there is no ${ctx(
      "shuttle",
      spaceShuttle
    )} docked. There is another door on the other side of a short hallway that leads to the rest of the station. You can see a red light on the panel next to the ${ctx(
      "station door",
      // airlockStationDoor
      this.stationDoor
    )}.`;
  }

  enter(prev: Room) {
    if (prev === spaceShuttle) {
      io.show(
        `With some effort you are able to slowly open the ${ctx(
          "airlock door",
          // spaceShuttleAirlockDoor
          this.shuttleDoor
        )}. It is heavy and rusted. When entering the ${ctx(
          "airlock",
          this
        )} you hear a metallic clank as the door swings shut behind you.`
      );
    }

    super.enter(prev);
  }
}

export default new Airlock();
