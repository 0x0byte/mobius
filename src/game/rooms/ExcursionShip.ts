import { Room } from "../../mobius";
import { Inspectable } from "../../actions";
// import Room from "../../rooms";

class ExcursionShip extends Room implements Inspectable {
  name = "Excursion Ship";

  inspect() {
    return `This ship ferried you to the edge of the Vault. It is beyond the effects of causality now from your perspective.`;
  }
}

export default new ExcursionShip();
