/**
 * Text decorations for important context
 */

const { AssertionError } = require("assert");

export function yellow(alias: string) {
  return `<span class="yellow">${alias}</span>`;
}

export function blue(alias: string) {
  return `<span class="blue">${alias}</span>`;
}

export function red(alias: string) {
  return `<span class="red">${alias}</span>`;
}

export function green(alias: string) {
  return `<span class="green">${alias}</span>`;
}

export function grey(alias: string) {
  return `<span class="grey">${alias}</span>`;
}

/**
 * Important text
 * @param alias String alias to be formatted
 * @returns HTML formatted string
 */
export function imp(alias: string) {
  return `<span class="imp">${alias}</span>`;
}

export function assert(condition: any, message?: string): asserts condition {
  if (!condition) {
    throw new AssertionError({ message });
  }
}
