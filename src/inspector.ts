// Main entrypoint for the debug inspector. Renders objects into inspector tree from game state.

// import { Game } from "./game/game";
import Mobius, { MobiusObject } from "./mobius";

// import airlock from "./game/rooms/Airlock";

// FIXME: any type for ctx
function objectComponent(ctx: any) {
  const el = document.createElement("a");
  el.className = "panel-block";
  el.innerHTML = `
    <span class="panel-icon ${ctx.isActive ? "is-active" : ""}">
      <i class="fas fas-book" aria-hidden="true"></i>
    </span>
    ${ctx.name}
  `;
  return el;
}

/**
 * Wraps a MobiusObject to display in the InspectorWindow
 */
class InspectorObject {
  constructor(obj: MobiusObject) {}
}

/**
 * Debug inspector window containing all the objects in the game.
 */
class InspectorWindow {}

export default class Inspector {
  // #rooms: Room[] = [];
  objects = document.getElementById("mobius-inspector-objects");

  constructor(game: Mobius) {
    console.log("Initializing inspector...");

    this.init();
  }

  init() {
    // initialize inspector window
    for (const obj of MobiusObject.objects) {
      this.objects?.appendChild(
        objectComponent({
          name: obj.name,
        })
      );
    }
  }
}
