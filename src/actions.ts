import io from "./io";
import { blue, imp } from "./utils";
import Mobius, { MobiusObject } from "mobius";

export type ActionResult = string;

// Actions should work like yarg commands and store their own help messages and sub command usage
export abstract class Action {
  abstract help(): string;

  getObject(args?: string[]): MobiusObject | undefined {
    return !args || args.length == 0
      ? undefined
      : Mobius.instance.context?.find(args.join(" "));
  }

  /**
   *
   * @param args arguments list used when performing this action
   */
  abstract execute(args?: string[]): ActionResult;
}

// helper actions so users don't have to re-write common actions

export interface Inspectable {
  inspect(): ActionResult;
}

export interface Usable {
  use(): ActionResult;
}

export interface Takeable {
  take(): ActionResult;
}

// NOTE: it would be nice to not rely on duck-typing and verify the function signature
// we must use ? to ensure we don't try to access the property of an undefined object due to the cast
export function isInspectable(obj: unknown): obj is Inspectable {
  return (obj as Inspectable)?.inspect !== undefined;
}

export function isUsable(obj: unknown): obj is Usable {
  return (obj as Usable)?.use !== undefined;
}

export function isTakeable(obj: unknown): obj is Takeable {
  return (obj as Takeable)?.take !== undefined;
}

// class HelpAction extends Action {
//   help() {
//     return "Help action help";
//   }

//   /**
//    * @param args Another action to display its help message.
//    */
//   execute(args?: string[]) {
//     io.show(this.help());

//     return "";
//   }
// }

class LookAction extends Action {
  help() {
    return "Look action help";
  }

  execute(args?: string[]): ActionResult {
    // if called without a direct object just inspect the room
    const object =
      !args || args.length == 0
        ? Mobius.instance.state.room
        : this.getObject(args);

    if (isInspectable(object)) {
      return object.inspect();
    } else {
      console.log({ object, args });
      return "You dont see anything like that to examine.";
    }
  }
}

class UseAction extends Action {
  help() {
    return "Use action help.";
  }

  execute(args?: string[]): ActionResult {
    const object = this.getObject(args);

    if (isUsable(object)) {
      return object.use();
    } else {
      console.log({ object, args });
      return "You're not sure how to perform that action.";
    }
  }
}

class TakeAction extends Action {
  help() {
    return "Take action help";
  }

  execute(): ActionResult {
    throw new Error("Unimplemented");
  }
}

// class InventoryAction extends Action {
//   help() {
//     return "Inventory action help.";
//   }

//   execute(): ActionResult {
//     throw new Error("Unimplemented");
//   }
// }

// action maps object

interface ActionMap {
  aliases: Set<string>;
  action: Action;
}

// NOTE: could potentially use an ES6 Map object instead of writting our own.
/**
 * NOTE: ActionCollection has undefined behavior when adding already existing aliases for other actions.
 */
class ActionCollection {
  #collection: ActionMap[] = [];

  add(action: Action, aliases: string[]) {
    if (aliases.length === 0) {
      throw new Error("Missing action aliases.");
    }

    // check if action aleady exists in collection
    const maybeMap = this.#collection.find((map) => map.action === action);
    if (maybeMap === undefined) {
      // create new action map
      this.#collection.push({
        aliases: new Set(aliases),
        action,
      });
    } else {
      // add to old action map
      maybeMap.aliases = new Set([...maybeMap.aliases, ...aliases]);
    }
  }

  perform(command: string): ActionResult {
    const args = command.split(" ");
    const alias = args.shift() ?? "";

    const map = this.#collection.find((map) => map.aliases.has(alias));

    if (map === undefined) {
      return `Unknown action ${alias}.`;
    }

    io.show(blue(`You ${alias} ${args.join(" ")}`));

    return map.action.execute(args);
  }
}

export class ActionContext {
  #map = new Map<string, MobiusObject>();

  /**
   * Sets an alias for obj. Will override existing aliases.
   * @param alias
   * @param obj
   */
  add(alias: string, obj: MobiusObject): void {
    alias = alias.toLowerCase().trim();
    this.#map.set(alias, obj);
  }

  find(alias: string): MobiusObject | undefined {
    alias = alias.toLowerCase().trim();
    return this.#map.get(alias);
  }
}

/**
 * Adds an alias for obj to the current rooms action context.
 * @param alias
 * @param obj
 */
export function ctx(alias: string, obj: MobiusObject): string {
  Mobius.instance.context?.add(alias, obj);
  return imp(alias);
}

// export const helpAction = new HelpAction();
export const lookAction = new LookAction();
export const useAction = new UseAction();
export const takeAction = new TakeAction();
// export const inventoryAction = new InventoryAction();

export default new ActionCollection();
