// General container for Mobius project configuration. Could be used by other developers using the framework to configure their project.
// TODO: should replace webpack config similar to how nuxt works.

const config = {
  debug: process.env.NODE_ENV == "development",

  devServer: {
    host: "localhost",
    port: 8080,
  },

  testServer: {
    host: "localhost",
    port: 8081,
  },
};

module.exports = () => {
  return config;
};
